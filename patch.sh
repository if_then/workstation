#!/bin/bash -e
#
# Author:: Lance Albertson <lance@osuosl.org>
# Copyright:: Copyright 2020, Cinc Project
# License:: Apache License, Version 2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

export TOP_DIR="$(pwd)"
export CHECKOUT_PATH="${TOP_DIR}/cinc-gems"

# This will patch a gem using Cinc branded patches
git_patch() {
  local arg project repo ref OPTIND
  while getopts "p:r:b:" arg ; do
    case ${arg} in
      p) project=${OPTARG} ;;
      r) repo=${OPTARG} ;;
      b) ref=${OPTARG} ;;
      *) return 1
    esac
  done
  if [ -n "$repo" ] ; then
    REPO="-r $repo"
  fi
  if [ -n "$ref" ] ; then
    REF=$ref
  else
    REF="$(get_ref ${project})"
  fi
  CINC_BRANCH=${REF:-stable/cinc}
  echo "Patching ${project} from ${CINC_BRANCH}..."
  if [ "${project}" == "omnibus-software" ] ; then
    cd $TOP_DIR/chef-workstation/omnibus
    CINC_BRANCH="stable/cinc"
  else
    cd $TOP_DIR/chef-workstation/components/gems
  fi
  ruby ${TOP_DIR}/scripts/checkout.rb -n ${project} ${REPO}
  cd ${CHECKOUT_PATH}/${project}
  git remote add -f --no-tags -t ${CINC_BRANCH} cinc https://gitlab.com/cinc-project/${project}.git
  git merge --no-edit cinc/${CINC_BRANCH}
}

get_ref() {
  if [ "${1}" == "omnibus-software" ] ; then
    cd $TOP_DIR/chef-workstation/omnibus
    echo "stable/cinc-$(ruby ${TOP_DIR}/scripts/checkout.rb -n $1 -s)"
  else
    cd $TOP_DIR/chef-workstation/components/gems
    echo "stable/cinc-$(ruby ${TOP_DIR}/scripts/checkout.rb -n $1 -s -r $1)"
  fi
}

source /home/omnibus/load-omnibus-toolchain.sh
# remove any previous builds
rm -rf $CHECKOUT_PATH ${TOP_DIR}/chef-workstation
mkdir -p $CHECKOUT_PATH
# Needed for git merge to function
git config --global user.email || git config --global user.email "maintainers@cinc.sh"
echo "Cloning ${REF:-master} branch from ${ORIGIN:-https://github.com/chef/chef-workstation.git}"
git clone -q -b ${REF:-master} ${ORIGIN:-https://github.com/chef/chef-workstation.git}
cd chef-workstation
if [ "${REF}" == "master" -o -z "${REF}" ] ; then
  CINC_REF="stable/cinc"
else
  CINC_REF="stable/cinc-${REF}"
fi
git remote add -f --no-tags -t ${CINC_REF} cinc https://gitlab.com/cinc-project/chef-workstation.git
git merge --no-edit cinc/${CINC_REF}
cd omnibus
git_patch -p omnibus-software
